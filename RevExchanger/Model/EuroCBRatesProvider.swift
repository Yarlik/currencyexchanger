//
//  EuroCBRatesProvider.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

class EuroCBRatesProvider: ExchangeRatesProvider {
    
    private let serverURL = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    private let euroCode = "EUR"
    private let currencyByCode: [String: Currency]
    private let baseCurrency: Currency
    
    init?(for currencies: [Currency]) {
        var groupedCurrencies = [String: Currency]()
        currencies.forEach {
            groupedCurrencies[$0.code] = $0
        }
        currencyByCode = groupedCurrencies
        
        guard let euro = currencyByCode[euroCode] else {
            return nil
        }
        
        baseCurrency = euro
    }
    
    func requestNewRates(completion: @escaping (CurrencyConverter?) -> Void) {
        Alamofire.request(serverURL).responseString { [weak self] response in
            switch response.result {
            case .failure(_):
                completion(nil)
            case .success(let value):
                completion(self?.parseServerResponse(responseString: value))
            }
        }
    }
    
    private func parseServerResponse(responseString: String) -> CurrencyConverter? {
        let xml = SWXMLHash.parse(responseString)
        var parsedRates = [Currency: Decimal]()
        
        let rates = xml["gesmes:Envelope"]["Cube"]["Cube"]["Cube"]
        
        for rateNode in rates.all {
            if let shortName = rateNode.element?.attribute(by: "currency")?.text,
                let currency = currencyByCode[shortName],
                let rateText = rateNode.element?.attribute(by: "rate")?.text,
                let rate = Double(rateText) {
                parsedRates[currency] = Decimal(rate)
            }
        }
        
        return EuroCBCurrencyConverter(baseCurrency: baseCurrency, rates: parsedRates)
    }
    
}
