//
//  Account.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

class Account {
    
    let currency: Currency
    var amount: Decimal
    
    init(currency: Currency, amount: Decimal) {
        self.currency = currency
        self.amount = amount
    }
    
}
