//
//  CurrencyConverter.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

protocol CurrencyConverter {
    
    func rate(from: Currency, to: Currency) -> Decimal?
    func convert(amount: Decimal, from: Currency, to: Currency) -> Decimal?
    
}
