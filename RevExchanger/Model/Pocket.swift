//
//  Pocket.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

class Pocket {
    
    fileprivate(set) var accounts = [Account]()
    
    func funds(for currency: Currency) -> Decimal? {
        return accounts.filter { $0.currency == currency }.first?.amount
    }
    
}

extension Pocket {
    
    class func defaultPocket(with currencies: [Currency] = Currency.supportedCurrencies, defaultAmount: Decimal = 100) -> Pocket {
        let pocket = Pocket()
        for currency in currencies {
            let newAccount = Account(currency: currency, amount: defaultAmount)
            pocket.accounts.append(newAccount)
        }
        return pocket
    }
    
}
