//
//  Currency.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

struct Currency {
    
    let id: Int
    let name: String
    let code: String
    let symbol: String
    let order: Int
    
}

extension Currency {
    
    static let supportedCurrencies: [Currency] = {
        guard let url = Bundle.main.url(forResource: "currencies", withExtension: "plist"),
            let currencyRecords = NSArray(contentsOf: url) as? [[String: Any]]  else {
                return [Currency]()
        }
        
        var currencies = [Currency]()
        for var record in currencyRecords {
            guard let id = record["id"] as? Int,
                let name = record["name"] as? String,
                let shortName = record["short_name"] as? String,
                let symbol = record["symbol"] as? String,
                let order = record["order"] as? Int else {
                    continue
            }
            
            currencies.append(Currency(id: id, name: name, code: shortName, symbol: symbol, order: order))
        }
        
        return currencies
    }()
    
}

extension Currency: Equatable {
    
    static func ==(lhs: Currency, rhs: Currency) -> Bool {
        return lhs.code == rhs.code
    }
    
}

extension Currency: Hashable {
    
    var hashValue: Int {
        return code.hashValue
    }
    
}

