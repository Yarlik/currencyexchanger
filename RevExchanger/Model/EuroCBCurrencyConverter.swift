//
//  EuroCBCurrencyConverter.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

class EuroCBCurrencyConverter: CurrencyConverter {
    
    private let baseCurrency: Currency
    private let rates: [Currency: Decimal]
    
    init(baseCurrency: Currency, rates: [Currency: Decimal]) {
        self.baseCurrency = baseCurrency
        self.rates = rates
    }

    func rate(from: Currency, to: Currency) -> Decimal? {

        guard baseCurrency != from else {
            return rates[to]
        }
        
        guard baseCurrency != to else {
            if let reverseRate = rate(from:to, to: from) {
                return 1 / reverseRate
            }
            return nil
        }
        
        guard let rateFrom = rates[from], rateFrom != 0, let rateTo = rates[to] else {
            return nil
        }
        
        return (1 / rateFrom) * rateTo
    }
    
    func convert(amount: Decimal, from: Currency, to: Currency) -> Decimal? {
        guard let rate = rate(from: from, to: to) else {
            return nil
        }
        return amount * rate
    }
    
}
