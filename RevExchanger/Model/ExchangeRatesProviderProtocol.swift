//
//  ExchangeRatesProvider.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

protocol ExchangeRatesProvider {
    
    func requestNewRates(completion: @escaping (CurrencyConverter?) -> Void)
    
}
