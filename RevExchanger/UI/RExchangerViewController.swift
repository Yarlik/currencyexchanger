//
//  ViewController.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import UIKit

class RExchangerViewController: UIViewController, InfiniteScrollViewDataSource {
    
    // MARK: - Properties
    
    private let content = RExchangerContent()
    
    @IBOutlet private weak var exchangeBarButton: UIBarButtonItem!
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var containerViewBottomConstraint: NSLayoutConstraint!

    @IBOutlet private weak var fromCurrencyScrollView: CurrencyScrollView!
    @IBOutlet private weak var toCurrencyScrollView: CurrencyScrollView!
    @IBOutlet private weak var noDataLabel: UILabel!
    
    private lazy var rateFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 4
        return formatter
    }()
    
    private lazy var fundsFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)

        configureContent()
        configureUI()
        
        content.startUpdatingRates()
    }
    
    // MARK: - Configuration
    
    private func configureContent() {
        content.onRatesUpdated = { [weak self] in
            self?.toCurrencyScrollView.reloadVisibleData()
            self?.fromCurrencyScrollView.reloadVisibleData()
        }
        
        content.onCurrentIndexChanged = { [weak self] type in
            guard let this = self else { return }
            this.scrollView(for: type.other).reloadVisibleData()
        }
        
        content.onActiveScrollChanged = { [weak self] type in
            guard let this = self else { return }
            this.scrollView(for: type).reloadVisibleData()
        }
        
        content.onAmountChanged = { [weak self] in
            self?.toCurrencyScrollView.reloadVisibleData()
            self?.fromCurrencyScrollView.reloadVisibleData()
        }
    }
    
    private func configureUI() {
        containerView.alpha = 0.0
        exchangeBarButton.isEnabled = false
        
        fromCurrencyScrollView.type = .from
        toCurrencyScrollView.type = .to
        
        configureScrollView(scrollView: fromCurrencyScrollView)
        configureScrollView(scrollView: toCurrencyScrollView)
        
        toCurrencyScrollView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        fromCurrencyScrollView.activate()
    }
    
    private func configureScrollView(scrollView: CurrencyScrollView) {
        scrollView.dataSource = self
        scrollView.pageIndicatorColor = UIColor.white.withAlphaComponent(0.2)
        scrollView.currentPageIndicatorColor = UIColor.white
        
        scrollView.onTapGesture = { scrollView in
            scrollView.activate()
        }
        
        scrollView.onDidBecomeActive = { [weak self] scrollView in
            self?.content.activeScroll = scrollView.type
        }
        
        scrollView.onAmountDidChanged = { [weak self] scrollView, amount in
            self?.content.amountChanged(for: scrollView.type, amount: amount)
        }
    }
    
    // MARK: - Helpers
    
    private func otherScrollView(scrollView: InfiniteScrollView) -> CurrencyScrollView {
        return scrollView == toCurrencyScrollView ? fromCurrencyScrollView : toCurrencyScrollView
    }
    
    private func scrollView(for type: ScrollType) -> CurrencyScrollView {
        return type == .from ? fromCurrencyScrollView : toCurrencyScrollView
    }
    
    // MARK: - Actions
    
    @IBAction func exchangeBarButtonTapped(_ sender: UIBarButtonItem) {
        do {
            try content.testExchangeOperation(shouldPerformExchange: true)
        } catch {
            let av = UIAlertController(title: "Sorry", message: "Operation is not allowed", preferredStyle: .alert)
            av.addAction(UIAlertAction(title: "OK", style: .default))
            present(av, animated: true)
        }
    }

    // MARK: - InfiniteScrollViewDataSource
    
    func numberOfItems(scrollView: InfiniteScrollView) -> Int {
        return content.accountsAvailable
    }
    
    func updateView(for scrollView: InfiniteScrollView, view: UIView, at index: Int) {
        guard let exchangeView = view as? CurrencyExchangeView, let currencyScrollView = scrollView as? CurrencyScrollView else {
            return
        }
        
        updatePage(scrollView: currencyScrollView, exchangeView: exchangeView, index: index)
    }
    
    private func updatePage(scrollView: CurrencyScrollView, exchangeView: CurrencyExchangeView, index: Int) {
        
        // currency code
        let currency = content.currency(at: index)
        exchangeView.currencyCodeLabel.text = currency.code
        
        // amount
        let amount = content.amount(for: scrollView.type, at: index)
        let currentAmount = exchangeView.enteredAmount
        if amount != currentAmount {
            exchangeView.enteredAmount = amount
        }
        
        // available funds
        let availableFunds = content.funds(for: currency) ?? 0
        exchangeView.availableFundsLabel.text = "You have \(currency.symbol)\(fundsFormatter.string(for: availableFunds) ?? "0")"
        
        if scrollView.type == .from,
            let amount = amount {
            exchangeView.availableFundsLabel.textColor = amount <= availableFunds ? UIColor.translucentWhite : UIColor.lightCoral
        }
        
        // rate
        exchangeView.rateLabel.text = content.buildRateString(formatter: rateFormatter, type: scrollView.type, currency: currency)
    }
    
    func currentPageChanged(scrollView: InfiniteScrollView, view: UIView, at index: Int) {
        guard let scrollView = scrollView as? CurrencyScrollView else { return }
        content.currentIndexChanged(for: scrollView.type, index: index)
    }
    
    func didReloadVisibleData(scrollView: InfiniteScrollView) {
        do {
            try content.testExchangeOperation()
            exchangeBarButton.isEnabled = true
        } catch {
            exchangeBarButton.isEnabled = false
        }
    }
    
    // MARK: - Keyboard
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt {
            
            containerViewBottomConstraint.constant = keyboardSize.height
            UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: {
                self.containerView.alpha = 1.0
                self.noDataLabel.alpha = 0.0
            })
        }
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
}
