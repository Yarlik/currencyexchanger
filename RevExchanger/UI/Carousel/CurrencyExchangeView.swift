//
//  CurrencyExchangeView.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import UIKit

class CurrencyExchangeView: UIView {
    
    var amountNumberFormatter: NumberFormatter!
    
    @IBOutlet weak var currencyCodeLabel: UILabel!
    @IBOutlet weak var availableFundsLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    
    var enteredAmount: Decimal? {
        get {
            return amountNumberFormatter.decimalMagnitude(from: amountTextField.text)
        }
        set {
            amountTextField.text = NumberFormatter.formatDecimalAsAmount(numberFormatter: amountNumberFormatter, amount: newValue)
        }
    }
    
}
