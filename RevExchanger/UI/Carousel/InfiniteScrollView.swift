//
//  InfiniteScrollView.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import UIKit

protocol InfiniteScrollViewDataSource: class {
    
    func numberOfItems(scrollView: InfiniteScrollView) -> Int
    func updateView(for scrollView: InfiniteScrollView, view: UIView, at index: Int)
    func currentPageChanged(scrollView: InfiniteScrollView, view: UIView, at index: Int)
    func didReloadVisibleData(scrollView: InfiniteScrollView)
    
}

class InfiniteScrollView: UIView, UIScrollViewDelegate {
    
    // MARK: - Public Properties
    
    weak var dataSource: InfiniteScrollViewDataSource? {
        didSet {
            reloadData()
        }
    }
    
    private(set) var currentIndex: Int {
        get {
            return pageControl.currentPage
        }
        set {
            pageControl.currentPage = newValue
        }
    }
    
    var currentPage: UIView? {
        let currentIndex = self.currentIndex
        for reusableSubview in reusableSubviews {
            if reusableSubview.tag == currentIndex {
                return reusableSubview
            }
        }
        return nil
    }
    
    var pageControlOffsetFromCenter: CGFloat = 61.0 {
        didSet {
            pageControl.center = CGPoint(x: pageControl.center.x, y: min(frame.height - pageControl.frame.size.height / 2, frame.height/2 + pageControlOffsetFromCenter))
        }
    }
    
    var pageIndicatorColor: UIColor? {
        set {
            pageControl.pageIndicatorTintColor = newValue
        }
        get {
            return pageControl.pageIndicatorTintColor
        }
    }
    
    var currentPageIndicatorColor: UIColor? {
        set {
            pageControl.currentPageIndicatorTintColor = newValue
        }
        get {
            return pageControl.currentPageIndicatorTintColor
        }
    }
    
    // MARK: - Private Properties
    
    private(set) var reusableSubviews = [UIView]()
    private var numberOfItems = 0
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = 0
        pageControl.currentPage = 0
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.pageIndicatorTintColor = .white
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.isUserInteractionEnabled = false
        
        addSubview(pageControl)
        return pageControl
    }()
    
    private lazy var scrollView: UIScrollView = {
        let sView = NoAutoScrolUIScrollView()
        sView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(sView)
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[sView]|",
            options: [],
            metrics: nil,
            views: ["sView": sView]
        ) )
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|[sView]|",
            options: [],
            metrics: nil,
            views: ["sView": sView]
        ) )
        sView.delegate = self
        sView.isPagingEnabled = true
        return sView
    }()
    
    // MARK: - Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        addReusableSubviews()
        bringSubview(toFront: pageControl)
        pageControl.currentPage = 0
    }
    
    private func addReusableSubviews() {
        for i in 0...2 {
            let customSubview = createReusableView() ?? UIView()
            scrollView.addSubview(customSubview)
            reusableSubviews.append(customSubview)
            customSubview.tag = i
        }
    }
    
    // MARK: - Public API
    
    func reloadData() {
        numberOfItems = dataSource?.numberOfItems(scrollView: self) ?? 0
        pageControl.numberOfPages = numberOfItems
        currentIndex = 0
        for (index, view) in reusableSubviews.enumerated() {
            view.tag = index
            dataSource?.updateView(for: self, view: view, at: index)
        }
        setNeedsLayout()
    }
    
    func reloadVisibleData() {
        for subview in reusableSubviews {
            dataSource?.updateView(for: self, view: subview, at: subview.tag)
        }
        dataSource?.didReloadVisibleData(scrollView: self)
    }
    
    // override this method to provide custom reusable subview
    func createReusableView() -> UIView? {
        return UIView()
    }
    
    // MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let contentSize = CGSize(width: CGFloat(reusableSubviews.count) * frame.size.width, height: frame.size.height)
        scrollView.contentSize = contentSize
        
        pageControl.center = CGPoint(x: frame.width / 2, y: min(frame.height - pageControl.frame.size.height / 2, frame.height/2 + pageControlOffsetFromCenter))
        
        let currentReusableSubview = scrollView.viewWithTag(pageControl.currentPage)
        
        for (index, view) in reusableSubviews.enumerated() {
            let origin = CGPoint(x: CGFloat(index) * bounds.width, y: 0)
            let size = bounds.size
            view.frame = CGRect(origin: origin, size: size)
        }
        
        if let currentReusableSubview = currentReusableSubview {
            scrollView.contentOffset.x = currentReusableSubview.frame.origin.x
            
        }
        
        recenterIfNecessary()
        updateCurrentPage()
    }
    
    private func recenterIfNecessary() {
        let currentOffset = scrollView.contentOffset
        let contentWidth = scrollView.contentSize.width
        let centerOffsetX = (contentWidth - bounds.width) / 2
        let distanceFromCenter = currentOffset.x - centerOffsetX
        let currentWidth = frame.width
        if fabs(distanceFromCenter) >= currentWidth/2 {
            let distanceToMove = CGFloat(ceil(distanceFromCenter / currentWidth) * currentWidth)
            scrollView.contentOffset = CGPoint(x: currentOffset.x - distanceToMove, y: currentOffset.y)
            for reusableSubview in reusableSubviews {
                reusableSubview.center.x = reusableSubview.center.x - distanceToMove
            }
        }
    }
    
    // MARK: - Scrolling
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        recenterIfNecessary()
        let visibleBounds = scrollView.bounds
        let minVisibleX = visibleBounds.minX
        let maxVisibleX = visibleBounds.maxX
        adjustSubviewPositions(minX: minVisibleX, maxX: maxVisibleX)
        updateCurrentPage()
    }
    
    private func updateCurrentPage() {
        let midVisible = CGPoint(x: scrollView.bounds.midX, y: scrollView.bounds.midY)
        for reusableSubview in reusableSubviews {
            if reusableSubview.frame.contains(midVisible) {
                if currentIndex != reusableSubview.tag {
                    currentIndex = reusableSubview.tag
                    dataSource?.currentPageChanged(scrollView: self, view: reusableSubview, at: reusableSubview.tag)
                }
                break
            }
        }
    }
    
    private func adjustSubviewPositions(minX: CGFloat, maxX: CGFloat) {
        guard numberOfItems > 0  else { return }
        
        if let lastSubview = reusableSubviews.last, lastSubview.frame.maxX < maxX {
            let first = reusableSubviews.removeFirst()
            first.frame.origin.x = lastSubview.frame.maxX
            reusableSubviews.append(first)
            first.tag = (first.tag + reusableSubviews.count) % numberOfItems
            dataSource?.updateView(for: self, view: first, at: first.tag)
        }
        
        if let firstSubview = reusableSubviews.first, firstSubview.frame.minX > minX {
            let last = reusableSubviews.removeLast()
            last.frame.origin.x = firstSubview.frame.minX - last.frame.width
            reusableSubviews.insert(last, at: 0)
            last.tag = (last.tag - (reusableSubviews.count) + numberOfItems) % numberOfItems
            dataSource?.updateView(for: self, view: last, at: last.tag)
        }
    }
    
}

// MARK: - Disable Autoscrolling

fileprivate class NoAutoScrolUIScrollView: UIScrollView {
    
    override func scrollRectToVisible(_ rect: CGRect, animated: Bool) {
        // this override is used to prevent autoscrolling
    }
    
}
