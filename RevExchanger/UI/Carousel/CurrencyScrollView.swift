//
//  CurrencyScrollView.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import UIKit

class CurrencyScrollView: InfiniteScrollView {
    
    // MARK: - Callbacks
    
    var onTapGesture: ((CurrencyScrollView)->())?
    var onAmountDidChanged: ((CurrencyScrollView, Decimal?)->())?
    var onDidBecomeActive: ((CurrencyScrollView)->())?
    
    // MARK: - Properties
    
    var type: ScrollType = .from {
        didSet {
            amountNumberFormatter.positivePrefix = type == .from ? "-" : "+"
        }
    }

    private(set) var isActive = false {
        didSet {
            guard isActive else {
                    return
            }
            onDidBecomeActive?(self)
        }
    }
    
    override var currentPage: CurrencyExchangeView? {
        return super.currentPage as? CurrencyExchangeView
    }
    
    private let amountNumberFormatter: NumberFormatter = {
        let formatter = NumberFormatter.amountNumberFormatter()
        formatter.positivePrefix = "-"
        return formatter
    }()
    
    // MARK: - Public
    
    func activate() {
        self.currentPage?.amountTextField.becomeFirstResponder()
    }
    
    // MARK: - Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTapGesture()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addTapGesture()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Gestures
    
    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func handleTap(gestureRecognizer: UIGestureRecognizer) {
        onTapGesture?(self)
    }
    
    // MARK: - Customization
    
    override func createReusableView() -> UIView? {
        let currencyView = UINib(nibName: "CurrencyExchangeView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? CurrencyExchangeView
        currencyView?.amountNumberFormatter = amountNumberFormatter
        
        if let textField = currencyView?.amountTextField {
            NotificationCenter.default.addObserver(self, selector: #selector(textDidChanged(notification:)), name: Notification.Name.UITextFieldTextDidChange, object: textField)
            NotificationCenter.default.addObserver(self, selector: #selector(textDidBeginEditing(notification:)), name: Notification.Name.UITextFieldTextDidBeginEditing, object: textField)
            NotificationCenter.default.addObserver(self, selector: #selector(textDidEndEditing(notification:)), name: Notification.Name.UITextFieldTextDidEndEditing, object: textField)
        }
        
        return currencyView
    }
    
    // MARK: - Notification
    
    @objc private func textDidChanged(notification: Notification) {
        guard let textField = notification.object as? UITextField else { return }
        let formattedText = NumberFormatter.formatStringAsAmount(numberFormatter: amountNumberFormatter, text: textField.text)
        for subview in reusableSubviews {
            (subview as? CurrencyExchangeView)?.amountTextField.text = formattedText
        }
        
        let amount = currentPage?.enteredAmount
        onAmountDidChanged?(self, amount)
    }
    
    @objc private func textDidBeginEditing(notification: Notification) {
        isActive = true
    }
    
    @objc private func textDidEndEditing(notification: Notification) {
        isActive = false
    }
    
    // MARK: - ScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if isActive, let currentPage = currentPage {
            currentPage.amountTextField.becomeFirstResponder()
        }
    }
    
}
