//
//  RExchangerContent.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

enum ScrollType {
    case from, to
    
    var other: ScrollType {
        return self == .from ? .to : .from
    }
}

enum ExchangeError: Error {
    case invalidOperation
    case invalidAmount
    case notEnoughFunds
    case sameCurrencyExchange
    case notConvertible
}

class RExchangerContent {
    
    // MARK: - Callbacks
    
    var onRatesUpdated: (()->())?
    var onCurrentIndexChanged: ((ScrollType)->())?
    var onActiveScrollChanged: ((ScrollType)->())?
    var onAmountChanged: (()->())?
    
    // MARK: - Properties
    
    private let pocket: Pocket
    
    private var amounts: [ScrollType: [Decimal?]]
    
    var activeScroll: ScrollType = .from {
        didSet {
            guard oldValue != activeScroll else { return }
            recalculateForNewActiveType()
        }
    }
    
    var currentIndex: [ScrollType: Int]
    
    private let provider: ExchangeRatesProvider!
    
    private var converter: CurrencyConverter? {
        didSet {
            print("Rates updated")
            onRatesUpdated?()
        }
    }
    
    private weak var ratesUpdateTimer: Timer?
    private let timerUpdateInterval: TimeInterval = 30.0
    
    // MARK: - Initialization
    
    init(pocket: Pocket = Pocket.defaultPocket(), provider: ExchangeRatesProvider! = EuroCBRatesProvider(for: Currency.supportedCurrencies)) {
        assert(provider != nil, "\(#function) - currency provider should not be nil")
        
        self.pocket = pocket
        self.provider = provider
        
        amounts = [ScrollType: [Decimal?]]()
        amounts[.from] = [Decimal?](repeating: nil, count: pocket.accounts.count)
        amounts[.to] = [Decimal?](repeating: nil, count: pocket.accounts.count)
        
        currentIndex = [ScrollType: Int]()
        currentIndex[.from] = 0
        currentIndex[.to] = 0
    }
    
    // MARK: - Public API
    
    var accountsAvailable: Int {
        return pocket.accounts.count
    }
    
    func currency(at index: Int) -> Currency {
        return pocket.accounts[index].currency
    }
    
    func funds(for currency: Currency) -> Decimal? {
        return pocket.funds(for: currency)
    }
    
    func account(at index: Int) -> Account {
        return pocket.accounts[index]
    }
    
    func amount(for type: ScrollType, at index: Int) -> Decimal? {
        return amounts[type]?[index]
    }
    
    func buildRateString(formatter: NumberFormatter, type: ScrollType, currency: Currency) -> String {
        guard let currentOtherIndex = currentIndex[type.other] else { return "" }
        let otherCurrency = pocket.accounts[currentOtherIndex].currency
        
        guard currency != otherCurrency,
            let rate = converter?.rate(from: currency, to: otherCurrency),
            let formattedRate = formatter.string(for: rate) else {
                return ""
        }
        
        return "\(currency.symbol)1 = \(otherCurrency.symbol)\(formattedRate)"
    }
    
    func testExchangeOperation(shouldPerformExchange: Bool = false) throws {
        guard let fromCurrentIndex = currentIndex[.from],
            let toCurrentIndex = currentIndex[.to] else {
                throw ExchangeError.invalidOperation
        }
        
        let fromAccount = account(at: fromCurrentIndex)
        let toAccount = account(at: toCurrentIndex)
        
        // user entered valid amount
        
        guard let amount = amounts[.from]?[fromCurrentIndex], amount > 0 else {
            throw ExchangeError.invalidAmount
        }
        
        // user has enough funds
        
        let currentFunds = fromAccount.amount
        if currentFunds < amount {
            throw ExchangeError.notEnoughFunds
        }
        
        // this amount is convertible
        
        let fromCurrency = fromAccount.currency
        let toCurrency = toAccount.currency
        
        guard fromCurrency != toCurrency,
            let convertedAmount = converter?.convert(amount: amount, from: fromCurrency, to: toCurrency) else {
                throw ExchangeError.notConvertible
        }
        
        if shouldPerformExchange {
            fromAccount.amount -= amount
            toAccount.amount += convertedAmount
            onAmountChanged?()
        }
    }
    
    // MARK: - Update
    
    func startUpdatingRates() {
        guard ratesUpdateTimer == nil else {
            return
        }
        let timer = Timer(fire: Date(), interval: timerUpdateInterval, repeats: false){ [weak self] timer in
            
            guard let this = self else {
                timer.invalidate()
                return
            }
            this.updateConverter()
        }
        
        RunLoop.main.add(timer, forMode: .commonModes)
        ratesUpdateTimer = timer
    }
    
    // MARK: Changes
    
    func currentIndexChanged(for type: ScrollType, index: Int) {
        currentIndex[type] = index
        if type == activeScroll {
            recalculateAmounts(for: type.other)
        }
        
        onCurrentIndexChanged?(type)
    }
    
    func amountChanged(for type: ScrollType, amount: Decimal?) {
        recalculateForAmountChanged(for: type, amount: amount)
        onAmountChanged?()
    }
    
    // MARK: - Private API
    
    private func updateConverter() {
        guard let provider = provider else {
            return
        }
        
        provider.requestNewRates { [weak self] newConverter in
            if let newConverter = newConverter {
                self?.converter = newConverter
            }
        }
    }
    
    // MARK: - Calculations
    
    private func recalculateForNewActiveType() {
        guard let currentIndex = currentIndex[activeScroll] else { return }
        let currentAmount = amounts[activeScroll]?[currentIndex]
        setCommonAmount(for: activeScroll, amount: currentAmount)
        onActiveScrollChanged?(activeScroll)
    }
    
    private func recalculateForAmountChanged(for type: ScrollType, amount: Decimal?) {
        setCommonAmount(for: type, amount: amount)
        recalculateAmounts(for: type.other)
    }
    
    private func recalculateAmounts(for type: ScrollType) {
        guard let fromCurrentIndex = currentIndex[type.other],
            var toAmounts = amounts[type] else {
            return
        }
        
        let fromAmount = amounts[type.other]?[fromCurrentIndex]
        let fromCurrency = pocket.accounts[fromCurrentIndex].currency
        
        for i in 0..<toAmounts.count {
            let toCurrency = pocket.accounts[i].currency
            if let fromAmount = fromAmount {
                toAmounts[i] = converter?.convert(amount: fromAmount, from: fromCurrency, to: toCurrency)
            } else {
                toAmounts[i] = nil
            }
        }
        amounts[type] = toAmounts
    }
    
    private func setCommonAmount(for type:ScrollType, amount: Decimal?) {
        guard var amountsToUpdate = amounts[type] else { return }
        for i in 0..<amountsToUpdate.count {
            amountsToUpdate[i] = amount
        }
        amounts[activeScroll] = amountsToUpdate
    }
    
}
