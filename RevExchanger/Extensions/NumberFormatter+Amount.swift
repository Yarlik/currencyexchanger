//
//  NumberFormatter+Amount.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 18/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    static func amountNumberFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.positivePrefix = formatter.plusSign
        formatter.negativePrefix = formatter.minusSign
        formatter.zeroSymbol = ""
        return formatter
    }
    
    static func formatDecimalAsAmount(numberFormatter: NumberFormatter, amount: Decimal?) -> String {
        guard let amount = amount else { return "" }
        let amountAsText = numberFormatter.string(for: amount)
        return formatStringAsAmount(numberFormatter: numberFormatter, text: amountAsText)
    }
    
    static func formatStringAsAmount(numberFormatter: NumberFormatter, text: String?) -> String {
        guard let text = text, !text.isEmpty else { return "" }
        
        let cleanedText = text.replacingOccurrences(of: numberFormatter.groupingSeparator, with: "")
        
        var integerPart: String = ""
        var fractionalPart: String = ""
        
        let components = cleanedText.components(separatedBy: numberFormatter.decimalSeparator)
        
        if components.count > 0 {
            integerPart = components[0]
        }
        
        if components.count > 1 {
            fractionalPart = components[1]
        }
        
        let notDigitSet = CharacterSet.decimalDigits.inverted
        fractionalPart = fractionalPart.components(separatedBy: notDigitSet).joined()
        integerPart = integerPart.components(separatedBy: notDigitSet).joined()
        
        numberFormatter.zeroSymbol = fractionalPart.isEmpty ? "" : "0"
        
        if let decimalInteger = Decimal(string: integerPart), !integerPart.isEmpty {
            if decimalInteger == 0 {
                integerPart = numberFormatter.positivePrefix + "0"
            } else {
                integerPart = numberFormatter.string(for: decimalInteger) ?? ""
            }
            
        } else {
            integerPart = ""
        }
        
        var result: String
        if fractionalPart.isEmpty {
            let hasFractionalPart = cleanedText.contains(numberFormatter.decimalSeparator)
            integerPart = integerPart.isEmpty && hasFractionalPart ? numberFormatter.positivePrefix + "0" : integerPart
            result = hasFractionalPart ? integerPart + numberFormatter.decimalSeparator : integerPart
        } else {
            if fractionalPart.count > numberFormatter.maximumFractionDigits {
                fractionalPart = String(fractionalPart.prefix(numberFormatter.maximumFractionDigits))
            }
            result = integerPart + numberFormatter.decimalSeparator + fractionalPart
        }
        
        return result
    }
    
    func decimalMagnitude(from string: String?) -> Decimal? {
        guard var string = string else { return nil }
        string = string.replacingOccurrences(of: self.groupingSeparator, with: "")
        return Decimal(string: string)?.magnitude
    }
    
}
