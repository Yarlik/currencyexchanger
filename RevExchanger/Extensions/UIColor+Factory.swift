//
//  UIColor+Factory.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 18/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let translucentWhite = UIColor.white.withAlphaComponent(0.9)
    static let translucentRed = UIColor.red.withAlphaComponent(0.8)
    static let lightCoral = UIColor(displayP3Red: 240/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    
}
