//
//  Decimal+Value.swift
//  RevExchanger
//
//  Created by Alexander Shchegryaev on 15/10/2017.
//  Copyright © 2017 Alexander Shchegryaev. All rights reserved.
//

import Foundation

extension Decimal {
    
    var doubleValue:Double {
        return NSDecimalNumber(decimal:self).doubleValue
    }
    
}
